package org.adempiere.project.callout;

import java.math.BigDecimal;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.GridTable;
import org.compiere.model.MClient;
import org.compiere.model.MProductPO;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;

public class CalloutMOrderLine implements IColumnCallout {

	@Override
	public String start(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value, Object oldValue) {
		if (mField.getColumnName().equals("M_AttributeSetInstance_ID")) {
			return updatePrice(ctx, WindowNo, mTab, mField, value);
		}
		return null;
	}

	private BigDecimal qty;

	public String updatePrice(Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value) {
		
		String trxName = null;
		if (   mTab != null
			&& mTab.getTableModel() != null) {
			GridTable gt = mTab.getTableModel();
			if (gt.isImporting()) {
				trxName = gt.get_TrxName();
			}
		}
		Integer C_BPartner_ID = (Integer) mTab.getValue("C_BPartner_ID");
		Integer M_Product_ID = (Integer) mTab.getValue("M_Product_ID");
		Integer AD_Client_ID = (Integer) mTab.getValue("AD_Client_ID");
		Integer M_AttributeSetInstance_ID = (Integer) mTab.getValue("M_AttributeSetInstance_ID");
		// ====================================================================================================================
		MClient client = new MClient(ctx, AD_Client_ID, trxName);
		if (client != null) {
			Boolean cek = (Boolean) client.get_Value("IsUseProductPurchasing");
			if (cek.equals(true)) {
				if (C_BPartner_ID != null && M_Product_ID != null && M_AttributeSetInstance_ID != null) {
					MProductPO productPO = new Query(ctx, MProductPO.Table_Name, "C_BPartner_ID =? AND M_Product_ID =? AND M_AttributeSetInstance_ID =?",
							trxName).setParameters(C_BPartner_ID, M_Product_ID, M_AttributeSetInstance_ID).first();
					if (productPO != null) {
						mTab.setValue("PriceEntered", productPO.getPriceLastPO());
						mTab.setValue("PriceActual", productPO.getPriceLastPO());
						mTab.setValue("PriceList", productPO.getPriceLastPO());
						mTab.setValue("C_Tax_ID", productPO.get_ValueAsInt("C_Tax_ID"));
					}
				}
			}

			// ==================================================================================================================
			Integer M_Warehouse_ID = (Integer) mTab.getParentTab().getValue("M_Warehouse_ID");
			String query = "SELECT SUM(c.qtyonhand) FROM m_storageonhand c JOIN m_locator d ON c.m_locator_id = d.m_locator_id WHERE c.m_product_id =? AND d.m_warehouse_id =?";
			qty = Env.ZERO;
			qty = DB.getSQLValueBD(trxName, query, M_Product_ID, M_Warehouse_ID);
			if (qty != null) {
				mTab.setValue("QtyOnHand", qty);
			} else {
				mTab.setValue("QtyOnHand", new BigDecimal(0));
			}
		}

		return null;
	}

}
